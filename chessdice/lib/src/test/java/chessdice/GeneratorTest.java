package chessdice;

import org.junit.Test;
import static org.junit.Assert.*;
/**
 * Tests for the generator.
 * @author Benjamin
 *
 */
public class GeneratorTest {
	/**
	 * Pick some numbers, ensure they are all in valid range.
	 */
	@Test public void testRannge() {
		int min = 0; int max = 5; //piece.size - 1
		for(int times = 0; times < 1000; times++) {
			int number = Generator.generate();
			assertTrue(number >= min && number <= max);
		}
	}
}
