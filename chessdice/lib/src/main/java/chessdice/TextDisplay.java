package chessdice;

import java.io.Console;
import java.io.IOException;

/**
 * Runs the chess dice program via the console.
 * 
 * @author Benjamin
 *
 */
public class TextDisplay implements Runnable {

	/**
	 * Is it whites turn to roll the dice?
	 */
	boolean isWhite;
	/**
	 * How many rolls should occur each turn?
	 */
	int rollsPerTurn;
	/**
	 * 
	 */
	final String message = " rolled: "; 
	
	/**
	 * Whether the current roll was successfully played in the corresponding chess game.
	 * Valid rolls are indicated by the next input line being empty.
	 */
	boolean isValid;
	
	/**
	 * The number of successful rolls.
	 */
	int times;
	
	/**
	 * The current turn number. A valid turn is comprised of two successful rolls.
	 * One roll for white and another roll for black.
	 */
	int turn;
	

	/**
	 * The console we are interacting with the user with.
	 */
	private Console console = System.console();

	@Override
	public void run() {
		try {
			configure();
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		loop();
	}

	/**
	 * Rolls the dice until user types exit.
	 * Entering an empty line changes the color.
	 * Entering a non-empty line rolls for the same color again.
	 */
	private void loop() {
		while(true) {
			clear();
			System.out.println(turn()+" "+player()+ message + roll());
			String in = getLine();
			if(in.isEmpty()) {
				isWhite = !isWhite;
				times++;
				isValid = true;
			} else {
				isValid = false;
			}
			if(in.equalsIgnoreCase("exit")) break;
		}
	}
	
	/**
	 * Returns the player.
	 * @return
	 */
	private String player() {
		return (isWhite) ? "White" : "Black";
	}
	
	/**
	 * Returns the turn number
	 * @return
	 */
	private int turn(){
		if(isValid && (times % 2 == 0)) {
			turn++;
		}
		return turn;
	}

	/**
	 * Check there is a console to interact with the user. Collect configuration
	 * info from the user.
	 * 
	 * @throws Exception Thrown if there is no console.
	 */
	private void configure() throws Exception {
		if (console == null)
			throw new Exception("No Console found.");
		while (true) {
			System.out.println("How many rolls per turn? [1,2,3]");
			try {
				rollsPerTurn = Integer.parseInt(getLine());
				if(rollsPerTurn < 1 || rollsPerTurn > 3) continue;
				break;
			} catch (NumberFormatException e) {} //ask again
		}
		turn = 1;
		times = 0;
		isWhite = true; 
	}

	/**
	 * Read a line of user input from the console.
	 * 
	 * @return A line that the user typed.
	 */
	private String getLine() {
		String answer = console.readLine();
		if (answer == null)
			throw new RuntimeException("Console didn't return any data.");
		return answer;
	}
	
	/**
	 * Clears the console. Doesn't work with the IDE console...
	 * @see "https://stackoverflow.com/questions/2979383/how-to-clear-the-console"
	 */
	private void clear() {
		if(System.getProperty("os.name").contains("Windows")) {
			try {
				new ProcessBuilder("cmd","/c","cls").inheritIO().start().waitFor();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.out.print("\033[H\033[2J"); 
			System.out.flush();  
		}
	}
	
	/**
	 * Roll dice the specified number of times.
	 * @return
	 *  Textual result of the rolls.
	 */
	private String roll() {
		StringBuilder ans = new StringBuilder();
		for(int i = 0; i < rollsPerTurn; i++) {
			ans.append(Generator.getPiece().pieceName()+" ");
		}
		return ans.toString();
	}
}
