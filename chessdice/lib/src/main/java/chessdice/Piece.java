package chessdice;
/**
 * The various Chess pieces
 * @author Benjamin
 *
 */
public enum Piece {
	/**
	 * 
	 */
	QUEEN{
		@Override
		String pieceName() {
			return "Queen";
		}
	},
	/**
	 * 
	 */
	PAWN{
		@Override
		String pieceName() {
			return "Pawn";
		}
	},
	/**
	 * 
	 */
	ROOK{
		@Override
		String pieceName() {
			return "Rook";
		}
	},
	/**
	 * 
	 */
	BISHOP{
		@Override
		String pieceName() {
			return "Bishop";
		}
	},
	/**
	 * 
	 */
	KING{
		@Override
		String pieceName() {
			return "King";
		}
	},
	/**
	 * 
	 */
	KNIGHT{
		@Override
		String pieceName() {
			return "Knight";
		}
	};
	
	/**
	 * Get the name of the piece.
	 * @return 
	 *  The name of the piece in plain English.
	 */
	abstract String pieceName();
}
