package chessdice;

import java.awt.CardLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.Map;
import java.util.TreeMap;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

/**
 * Runs the chess dice program through a java SWING GUI.
 * @author Benjamin
 *
 */
public class GUIDisplay implements Runnable{

	/**
	 * The frame displaying the program to the user.
	 */
	private JFrame frame;
	
	/**
	 * The number of dice that should be displayed.
	 */
	volatile int diceCount;
	
	/**
	 * The turn number in the chess game.
	 */
	volatile int turnNumber;
	
	/**
	 * who's turn it is.
	 * true = white's turn.
	 * false = black's turn.
	 */
	volatile boolean isWhite;
	
	/**
	 * The panel that is shown to the user during the normal operation of the program.
	 */
	private JPanel mainPanel;
	
	/**
	 * The panel the user is shown on program start.
	 * Asks for some config information, and has a start button.
	 */
	private JPanel startPanel;
	
	/**
	 * The images of the white pieces.
	 */
	private Map<Piece, Icon> whitePieces;
	
	/**
	 * The images of the black pieces.
	 */
	private Map<Piece, Icon> blackPieces;
	
	/*
	 * Plan:
	 * init method -> load the images
	 * initial screen -> select how many dies [1,2,3] + BEGIN button
	 * 
	 * two buttons -> roll again && re-roll
	 * A text box: move number + who's color it is
	 * 
	 * 'x' number of images
	 * 
	 * 							- X
	 * ============================
	 * =TEXT                      =
	 * =                          =
	 * =IMAGE IMAGE IMAGE  reroll =
	 * =IMAGE IMAGE IMAGE         =
	 * =IMAGE IMAGE IMAGE   roll  =
	 * =                          =
	 * ============================
	 */
	
	
	@Override
	public void run() {
		System.out.println("GUI!");
		try {
			SwingUtilities.invokeAndWait(()->{
				initialize();
			});
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Initializes the display.
	 */
	private void initialize() {
		whitePieces = new TreeMap<>();
		blackPieces = new TreeMap<>();
		loadImages();
		isWhite = true;
		turnNumber = 1;
		//CREATE COMPONENTS
		frame = new JFrame();
		frame.setTitle("Chess Dice");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		startPanel = new StartPanel();
		mainPanel = new MainPanel();
		
		//LAYOUT MANAGEMENT
		frame.getContentPane().setLayout(new CardLayout());
		
		//ADD COMPONENTS
		frame.getContentPane().add(startPanel);
		frame.getContentPane().add(mainPanel);
		
		//MAKE VISIBLE
		frame.pack();
		frame.setVisible(true);
	}

	/**
	 * Loads the images.
	 */
	private void loadImages() {
		try {
			for(Piece p : Piece.values()) {				
				whitePieces.put(p, new ImageIcon(ImageIO.read(GUIDisplay.class.getResource("/"+p.pieceName().toLowerCase()+"_white.png")).
						getScaledInstance(100, 100, Image.SCALE_SMOOTH)));
				blackPieces.put(p, new ImageIcon(ImageIO.read(GUIDisplay.class.getResource("/"+p.pieceName().toLowerCase()+"_black.png")).
						getScaledInstance(100, 100, Image.SCALE_SMOOTH)));
				
				/*whitePieces.put(p, new ImageIcon(ImageIO.read(new File("src/main/resources/"+p.pieceName().toLowerCase()+"_white.png")).
						getScaledInstance(100, 100, Image.SCALE_SMOOTH)));
				blackPieces.put(p, new ImageIcon(ImageIO.read(new File("src/main/resources/"+p.pieceName().toLowerCase()+"_black.png")).
						getScaledInstance(100, 100, Image.SCALE_SMOOTH)));*/
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Panel that asks user how many dies 
	 * and has a start button.
	 * @author Benjamin
	 *
	 */
	private class StartPanel extends JPanel {

		/**
		 * field for (unused) serialization.
		 */
		private static final long serialVersionUID = 654984511211L;
		
		/**
		 * Store the user input.
		 */
		int numberOfDice = 1;
		
		/**
		 * Constructor.
		 */
		StartPanel(){
			//Text field to tell the user what the program is asking.
			this.add(new JTextArea("How many dice?") {
				private static final long serialVersionUID = 62485293L;
				{
					setEditable(false);
				}
			});
			
			//Collect user input through the JComboBox.
			this.add(new JComboBox<Integer>(new Integer[] {1,2,3}) {
				private static final long serialVersionUID = 9841354L;
				{
					addItemListener((e)->{
						numberOfDice = (Integer) e.getItem();
					});
				}
			});
			
			//Start button
			this.add(new JButton("Start") {
				private static final long serialVersionUID = 9876541564L;
				{
					addActionListener((ActionEvent e)->{
						diceCount = numberOfDice; //pass the user input to the parent class.
						((MainPanel)mainPanel).rollAgain(); //tell the main panel to roll dice for first roll.
						//Tell frame's layout manager to change to the next JPanel.
						CardLayout layout = (CardLayout) frame.getContentPane().getLayout();
						Container container = frame.getContentPane();
						layout.next(container);
					});
				}
			});
			
			this.setPreferredSize(new Dimension(250, 50));
		}
		
	}
	
	/**
	 * Panel that shows the dies and has buttons to roll and re-roll.
	 * A string displays the move number the game is on.
	 * @author Benjamin
	 *
	 */
	private class MainPanel extends JPanel{

		/**
		 * field for (unused) serialization.
		 */
		private static final long serialVersionUID = 7946488722831897L;
		
		/**
		 * Text field to show the current move number + who's turn it is.
		 */
		private JTextArea text;
		/**
		 * Button to roll the next color.
		 */
		private JButton roll;
		/**
		 * Button to re-roll for the current color.
		 */
		private JButton reroll;
		
		/**
		 * Remember if white had a valid roll.
		 */
		private boolean whitePassed;
		
		/**
		 * Remember if black had a valid roll.
		 */
		private boolean blackPassed;
		
		/**
		 * The pieces that this JPanel should display.
		 * Modified by the isWhite flag to draw them in white or black.
		 */
		private Piece[] pieces = new Piece[3];
		
		/**
		 * The labels used to show the images of pieces.
		 */
		private JLabel[] drawings = new JLabel[3];
		
		/**
		 * Constructor.
		 */
		MainPanel(){
			//ADD LAYOUT MANAGER TODO
			
			//ADD COMPONENTS
				//text field
				text = new JTextArea();
				text.setEditable(false);
				this.add(text);
				//buttons
				roll = new JButton("Roll") {
					private static final long serialVersionUID = 8941532641L;
					{
						addActionListener((ActionEvent e)-> rollSuccess());
					}
				};
				
				reroll = new JButton() {
					private static final long serialVersionUID = 3281564L;
					{
						addActionListener((ActionEvent e)-> rollFail());
					}
				};
				this.add(roll);
				this.add(reroll);
				//Image displayer 
				for(int i = 0; i < 3; i++) {
					drawings[i] = new JLabel(); 
					this.add(drawings[i]);
				}
				//TEMP - text display - used for debugging
				/*for(int i = 0; i < 3; i++) {
					final int n = i;
					this.add(new JTextArea() {
						private static final long serialVersionUID = 98754L;
						private final int id;
						{
							setEditable(false);
							id = n;
						}
						public void paint(Graphics g) {
							super.paint(g);
							if(pieces[id] != null) {
								setText(pieces[id].pieceName());
							} else {
								setText("empty");
							}
						}
					});
				}*/
			this.setPreferredSize(new Dimension(650, 120)); 
			this.validate();
		}
		
		@Override
		public void paint(Graphics g) {
			super.paint(g);
			reroll.setText("Reroll for "+(isWhite ? "White" : "Black"));
			text.setText("Move number: "+turnNumber);
			for(int i = 0; i < 3; i++) {
				if(pieces[i] == null) continue;
				if(isWhite) {
					drawings[i].setIcon(whitePieces.get(pieces[i]));
				} else {
					drawings[i].setIcon(blackPieces.get(pieces[i]));
				}
				drawings[i].validate();
			}
		}
		
		/**
		 * Called when the roll allowed a valid move in the chess game.
		 * Increments move count.
		 * Updates active color.
		 * Rolls for new pieces.
		 */
		private void rollSuccess() {
			if(isWhite) {
				whitePassed = true;
			} else {
				blackPassed = true;
			}
			isWhite = !isWhite;
			if(whitePassed && blackPassed) {
				turnNumber++;
				whitePassed = false;
				blackPassed = false;
			}
			rollAgain();
			repaint();
		}
		
		/**
		 * Called when roll did not produce any valid moves for the chess game.
		 * Rolls for new pieces.
		 */
		private void rollFail() {
			if(isWhite) {
				whitePassed = false;
			} else {
				blackPassed = false;
			}
			rollAgain();
			repaint();
		}
		
		/**
		 * Rolls the chess dice again.
		 * updates the pieces field with new pieces.
		 */
		private void rollAgain() {
			for(int i = 0; i < diceCount; i++) {
				pieces[i] = Generator.getPiece();
			}
		}
	}
}
