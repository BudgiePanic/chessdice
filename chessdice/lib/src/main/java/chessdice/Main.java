package chessdice;

/**
 * Main program entry point
 * @author Benjamin
 *
 */
public class Main {

	/**
	 * Read program args and start either a GUI display, or text display.
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length == 0) {
			new GUIDisplay().run();
		}
		else if(args[0].equalsIgnoreCase("text")) {
			new TextDisplay().run();
		}
	}

}
