package chessdice;
/**
 * Random number provider.
 * @author Benjamin
 *
 */
public class Generator {
	
	public static final int MAX = Piece.values().length - 1;
	private static final int MIN = 0;
	
	/**
	 * Generates a random number between 1 and Piece.size[].
	 * @see "https://www.javatpoint.com/how-to-generate-random-number-in-java"
	 * @return
	 *  A number between 0 and Piece.size - 1;
	 */
	public static int generate() {
		return (int) (Math.random() * (MAX - MIN + 1) + MIN);
	}
	
	/**
	 * Picks a random piece.
	 * @return
	 *  A random piece.
	 */
	public static Piece getPiece() {
		return Piece.values()[generate()];
	}
}
